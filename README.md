# Flectra Community / l10n-germany

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[l10n_de_country_states](l10n_de_country_states/) | 2.0.1.0.0| German Country States
[l10n_de_toponyms](l10n_de_toponyms/) | 2.0.1.0.0| German Toponyms
[l10n_de_tax_statement_zm](l10n_de_tax_statement_zm/) | 2.0.1.0.0| German VAT Statement Extension
[l10n_de_location_nuts](l10n_de_location_nuts/) | 2.0.1.0.0| NUTS specific options for German
[l10n_de_skr03_mis_reports](l10n_de_skr03_mis_reports/) | 2.0.1.0.0|         MIS Builder templates for the German P&L        and Balance Sheets (SKR03)
[l10n_de_skr04_mis_reports](l10n_de_skr04_mis_reports/) | 2.0.1.0.0|         MIS Builder templates for the German P&L        and Balance Sheets (SKR04)
[l10n_de_tax_statement](l10n_de_tax_statement/) | 2.0.1.0.0| German VAT Statement
[l10n_de_holidays](l10n_de_holidays/) | 2.0.1.0.0| Holidays for Germany
[l10n_de_steuernummer](l10n_de_steuernummer/) | 2.0.1.0.0| German SteuerNummer validation


